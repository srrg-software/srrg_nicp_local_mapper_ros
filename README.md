# srrg_nicp_local_mapper_ros

ROS version of [srrg_nicp_local_mapper](https://gitlab.com/srrg-software/srrg_nicp_local_mapper)

## Prerequisites

This package requires:
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss)
* [srrg_core_ros](https://gitlab.com/srrg-software/srrg_core_ros)
* [srrg_core_map](https://gitlab.com/srrg-software/srrg_core_map)
* [srrg_core_map_ros](https://gitlab.com/srrg-software/srrg_core_map_ros)
* [srrg_nicp_tracker](https://gitlab.com/srrg-software/srrg_nicp_tracker)
* [srrg_nicp_tracker_ros](https://gitlab.com/srrg-software/srrg_nicp_tracker_ros)
* [srrg_nicp_local_mapper](https://gitlab.com/srrg-software/srrg_nicp_local_mapper)

## Nodes
* `srrg_nicp_local_mapper_node`

Type `-h` for help

    rosrun srrg_nicp_local_mapper_ros srrg_nicp_local_mapper_node -h


## Authors
* Jacopo Serafin
* Giorgio Grisetti

## Related Publications

* Jacopo Serafin and Giorgio Grisetti. "[**Using extended measurements and scene merging for efficient and robust point cloud registration**](http://www.sciencedirect.com/science/article/pii/S0921889015302712)", Robotics and Autonomous Systems, 2017.
* Jacopo Serafin and Giorgio Grisetti. "[**NICP: Dense Normal Based Point Cloud Registration.**](http://ieeexplore.ieee.org/document/7353455/)" In Proc. of the International Conference on Intelligent Robots and Systems (IROS), Hamburg, Germany, 2015.
