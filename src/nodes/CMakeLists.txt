add_executable(srrg_nicp_local_mapper_node srrg_nicp_local_mapper_node.cpp)
target_link_libraries(srrg_nicp_local_mapper_node 
  srrg_nicp_local_mapper_ros_library
  ${catkin_LIBRARIES}
)
