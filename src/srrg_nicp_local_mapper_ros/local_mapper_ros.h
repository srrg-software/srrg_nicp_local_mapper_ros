#pragma once

#include <srrg_nicp_tracker/tracker.h>
#include <srrg_nicp_tracker/base_triggers.h>
#include <srrg_nicp_local_mapper/local_mapper.h>
#include <srrg_nicp_tracker/multi_tracker.h>
#include <srrg_messages/tf_overrider_trigger.h>
#include <srrg_ros_wrappers/image_message_listener.h>
#include <srrg_messages/message_enlister_trigger.h>
#include <srrg_messages/message_dumper_trigger.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_core_map_ros/map_msgs_ros.h>
#include <srrg_core_ros/IdsSrv.h>
#include <srrg_core_ros/LocalMapByIdSrv.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <tf/transform_listener.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <vector>
#include <fstream>
#include <iostream>

namespace srrg_nicp_local_mapper_ros {

  class LocalMapperRos : public srrg_nicp_local_mapper::LocalMapper {
  public:
  LocalMapperRos(srrg_nicp_tracker::Tracker* tracker,
		 int priorory = 10,
		 srrg_boss::Serializer* ser=0,
		 srrg_boss::IdContext * context=0) :    
    srrg_nicp_local_mapper::LocalMapper(tracker,
					srrg_nicp_tracker::Tracker::TRACK_GOOD|
					srrg_nicp_tracker::Tracker::TRACK_BROKEN|
					srrg_nicp_tracker::Tracker::REFERENCE_FRAME_RESET|
					srrg_nicp_tracker::Tracker::TRACKING_DONE|
					srrg_nicp_tracker::Tracker::NEW_CAMERA_ADDED,
					priorory,
					ser) {
      _context = context;
      std::cerr << "local mapper ros" << std::endl;
    }
  
    void init(ros::NodeHandle& nh);
    virtual void onCameraInfoCreated(srrg_core::BaseCameraInfo* camera_info);
    virtual void onLocalMapCreated(srrg_core_map::LocalMap* lmap);
    virtual void onNewNodeCreated(srrg_core_map::MapNode* node, srrg_core_map::BinaryNodeRelation* rel);
    virtual void onRelationCreated(srrg_core_map::BinaryNodeRelation* rel);

    virtual bool srvLocalMapIds(srrg_core_ros::IdsSrv::Request& req, srrg_core_ros::IdsSrv::Response& resp);
    virtual bool srvGetLocalMap(srrg_core_ros::LocalMapByIdSrv::Request& req, srrg_core_ros::LocalMapByIdSrv::Response& resp);

  protected:
    void serializeCameras();
    void serializeCamera(srrg_core::BaseCameraInfo* cam);
    void serializeTrajectory(srrg_core_map::MapNodeList& nodes);
    void serializeNode(srrg_core_map::MapNode* n);
    void serializeRelation(srrg_core_map::BinaryNodeRelation* rel);
    std::vector<srrg_core::BaseCameraInfo*> _camera_infos;
    ros::Publisher _image_node_pub;
    ros::Publisher _multi_image_node_pub;
    ros::Publisher _camera_info_pub;
    ros::Publisher _multi_camera_info_pub;
    ros::Publisher _local_map_pub;
    ros::Publisher _relations_pub;
    ros::ServiceServer _local_map_ids_srv;
    ros::ServiceServer _get_local_map_srv;
    srrg_boss::IdContext* _context;

    std::map<int, srrg_core_ros::LocalMapMsg*> _savedLocalMapMsgs;
  };
  
}
